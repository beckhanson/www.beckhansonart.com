<?php

  //BEGIN FORM VALIDATION
  
  //set up variables:

  $lab_name = "";
  $lab_email = "";
	
  $valid_form = false;		
  
$labNameError = "";
$labEmailError = "";

 if( isset($_POST['form_submit']) )
	{
		//process form data	
		echo "<h1>Form has been submitted and should be processed</h1>";
		
		$lab_name = $_POST['lab_name'];
		$lab_email = $_POST['lab_email'];
		

		$valid_form = true;

function validateName($lab_name){
  
        //validate name - Cannot be empty
        if( empty($lab_name)) {
          $labNameError = "Please enter a name";
          $valid_form = false;
        }
}

function validateEmail($lab_email){
 
	//validate email using PHP filter
	if( !filter_var($lab_email, FILTER_VALIDATE_EMAIL)) {
		$labEmailError = "Invalid email";
		$valid_form = false;	
  }
}

if( !validateName($lab_name) ) {
  $valid_form = false;
  $labNameError = "Please enter your name";
}		
   

    
    if($valid_form) {
    
   // validateName();
    //validateEmail();
    
   // if(!empty($validateName))
		//{
			//echo "Nope - try again";
    //}
  }

//else //display the empty form 
//{
  }

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>WDV341 Intro PHP</title>

<style>
.error	{
	color:red;
	font-style:italic bold;	
}
</style>
 <script src="https://www.google.com/recaptcha/api.js" async defer></script>  
 <!-- RE-CAPTCHA -->
</head>

<body>
<h1>WDV341 Intro PHP</h1>
<h2>Unit-7 and Unit-8 Form Validations and Self Posting Forms.</h2>
<h3>In Class Lab - Self Posting Form</h3>
<p><strong>Instructions:</strong></p>
<ol>
  <li>Modify this page as needed to convert it into a PHP self posting form.</li>
  <li>Use the validations provided on the page for the server side validation. You do NOT need to code any validations.</li>
  <li>Modify the page as needed to display any input errors.</li>
  <li>Include some form of form protection.</li>
  <li>You do NOT need to do any database work with this form. </li>
</ol>
<p>When complete:</p>
<ol>
  <li>Post a copy on your host account.</li>
  <li>Push a copy to your repo.</li>
  <li>Submit the assignment on Blackboard. Include a link to your page and to your repo.</li>
</ol>

<?php

if ($valid_form)			//If the form info is valid
{
?>
	<h3>Thank You!</h3>
    <p>Your information has been registered!</p>

<?php
}	//end the true branch of the form view area
else
{

?>

<form id="form1" name="form1" method="post" action="lab-self-posting-form.php">
  <p>
    <label for="lab_name">Name:</label>
    <input type="text" name="lab_name" id="lab_name" value="<?php echo trim($lab_name); ?>">
    <span id="errorName" class="error"><?php echo $labNameError; ?></span>
  </p>
  <p>
    <label for="lab_email">Email:</label>
    <input type="text" name="lab_email" id="lab_email" value="<?php echo $lab_email; ?>">
    <span id="errorEmail" class="error"><?php echo $labEmailError; ?></span>
  </p>
  <div class="g-recaptcha" name="inReCaptcha" id="inReCaptcha" data-sitekey="6Lf_GHQUAAAAAIBzU9tOxIgBNxDmn_mZ2LEz3_fg"></div>
 
  <p>
    <input type="submit" name="form_submit" id="form_submit" value="Submit">
    <input type="reset" name="button2" id="button2" value="Start Over">
  </p>
</form>

<?php
	}	//end else branch for the View area
 ?>
  


<p>&nbsp;</p>
</body>
</html>
