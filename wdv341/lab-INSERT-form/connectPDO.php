<?php
$serverName = "localhost";
$username = "root";
$password = "root";
$dbname = "wdv341";
$sql = "";

try {
    $conn = new PDO("mysql:host=$serverName;dbname=wdv341", $username, $password);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Connected successfully"; 
    }
catch(PDOException $e)
    {
    echo "Connection failed: " . $e->getMessage();
    }
?>