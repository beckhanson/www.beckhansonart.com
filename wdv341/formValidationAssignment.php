<?php 
//set up variables:

$inName = "";
$inSSN = "";
$inOption = "";


$validForm = false;

$nameErrorMsg = "";
$ssnErrorMsg = "";
$optionErrorMsg = "";


function validateName(){
	global $inName, $nameErrorMsg, $validForm; //bring in variables
		$nameErrorMsg="";					   // clear the error message
	if($inName=="") {						  //if the name is empty, show the error msg
		$validForm = false;
		$nameErrorMsg = "Name field is required";
	}

}

function validateSSN(){
	global $inSSN, $ssnErrorMsg, $validForm;
	$nameErrorMsg="";
	$ssnFormat = "#^\d{9}$#";
	if($inSSN=="") //validates that the field isnt blank 
		{
			$validForm=false;
			$ssnErrorMsg = "Social Security Number is required";
		}
	
	elseif(intVal($inSSN)==0)		 //validates that a number was entered	
		{
			$validForm = false;
			$ssnErrorMsg= "Must enter a number";
		}
	
	elseif(!preg_match($ssnFormat, $inSSN)) //validates the number field
		{
			$validForm= false;
			$ssnErrorMsg = "Must enter a valid Social Security Number. 9 digits, no spaces.";
		}
	}

	function validateOption(){
		global $inOption, $optionErrorMsg, $validForm;
		$optionErrorMsg = "";
		
		if($inOption == "")
		{
			$validForm = false;
			$optionErrorMsg = "Please select an option";
		}
    }
    
    
	

if(isset($_POST["submit"])) //if the form has been submitted do the following: 
{
	
	//get the name/value pairs from POST and store them in variables. 
	
	$inName = trim($_POST['inName']);
	$inSSN = $_POST['inSSN'];
	$inOption = $_POST['inOption'];
	
	
	$validForm= true;
	
	validateName();
	validateSSN();
    validateOption();
 
	if(!empty($validateOption))
		{
			echo "Nope - try again";
		}
		
}
	
else //display the empty form 
{

}

?>

<!DOCTYPE html>
<html >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WDV341 Intro PHP - Form Validation Example</title>
<style>

@import url('https://fonts.googleapis.com/css?family=Amatic+SC');

* {
    font-family: 'Amatic SC', cursive;
}
body{
    font-family: 'Quicksand', sans-serif;
    background-image: url("http://www.beckhansonart.com/images/fireandicecropbg.jpg");
}


#orderArea	{
	width:600px;
  background-color: #00e4bc;
  font-size: 32px;
  margin: auto;
  padding: 15px;
}

h2 {
  background-color: violet;
}

h3 {
  background-color: violet;
}

input {
  font-size: 32px;
}

.error	{
	color:red;
	font-style:italic bold;	
}
</style>
 <script src="https://www.google.com/recaptcha/api.js" async defer></script>  
 <!-- RE-CAPTCHA -->
</head>

<body>

<div id="orderArea">
<h2>WDV341 Intro PHP</h2>
<h3>Form Validation Assignment</h3>

<?php

if ($validForm)			//If the form info is valid
{
?>
	<h3>Thank You!</h3>
    <p>Your information has been registered!</p>

<?php
}	//end the true branch of the form view area
else
{
?>

  <form id="form1" name="form1" method="post" action="formValidationAssignment.php">
  <h3>Customer Registration Form</h3>
  <table width="587" border="0">
    <tr>
      <td width="117">Name:</td>
      <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo trim($inName); ?>"/></td>
      <td width="210" class="error"> <? php echo $nameErrorMsg; ?></td>
    </tr>
    <tr>
      <td>Social Security</td>
      <td><input type="text" name="inSSN" id="inSSN" size="40" value="<?php echo $inSSN; ?>" /></td>
      <td class="error"> <?php echo $ssnErrorMsg; ?></td>
    </tr>
    <tr>
      <td>Choose a Response</td>
      <td><p>
        <label>
          <input type="radio" name="inOption" id="inOption1" value="Phone" <?php if ($inOption == 'Phone') { echo "checked"; } ?> />
          Phone</label>
        <br>
        <label>
          <input type="radio" name="inOption" id="inOption2" value="Email" <?php if ($inOption == 'Email') { echo "checked"; } ?>/>
          Email</label>
        <br>
        <label>
          <input type="radio" name="inOption" id="inOption3" value="US Mail" <?php if ($inOption == 'US Mail') { echo "checked"; } ?>/>
          US Mail</label>
        <br>
      </p></td>
      <td class="error"><?php echo $optionErrorMsg; ?></td>
    </tr>
  </table>
  <p>
  <div class="g-recaptcha" name="inReCaptcha" id="inReCaptcha" data-sitekey="6Lf_GHQUAAAAAIBzU9tOxIgBNxDmn_mZ2LEz3_fg"></div>
  <div class="error"> </div>
  <br>
    <input type="submit" name="submit" id="button" value="Register" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>

	<?php
	}	//end else branch for the View area
	?>

</div>

</body>
</html>